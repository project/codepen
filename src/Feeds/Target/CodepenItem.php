<?php

namespace Drupal\codepen\Feeds\Target;

use Drupal\feeds\Feeds\Target\StringTarget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\feeds\FieldTargetDefinition;

/**
 * Defines a Codepen field mapper.
 *
 * @FeedsTarget(
 *   id = "codepen",
 *   field_types = {
 *     "codepen",
 *   }
 * )
 */
class CodepenItem extends StringTarget {

  /**
   * {@inheritdoc}
   */
  protected static function prepareTarget(FieldDefinitionInterface $field_definition) {
    return FieldTargetDefinition::createFromFieldDefinition($field_definition)
      ->addProperty('input')
      ->addProperty('codepen_html')
      ->addProperty('codepen_css')
      ->addProperty('codepen_js')
      ->addProperty('codepen_result')
      ->addProperty('codepen_id')
      ->addProperty('user_id');
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    // The Codepen URL (input), User ID and Codepen ID are required for the field value
    // to work. If the feed only provided one, generate the others.
    if (!empty($values['input']) && empty($values['codepen_id']) && empty($values['user_id'])) {
      $values['codepen_id'] = codepen_get_codepen_id($values['input']);
      $values['user_id'] = codepen_get_user_id($values['input']);
    }
    elseif (empty($values['input']) && !empty($values['codepen_id']) && !empty($values['user_id'])) {
      $values['input'] = 'https://www.codepen.io/' .  $values['user_id'] . '/pen/' . $values['codepen_id'];
    }
  }

}
