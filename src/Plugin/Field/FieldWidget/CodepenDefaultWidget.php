<?php

namespace Drupal\codepen\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'codepen_default' widget.
 *
 * @FieldWidget(
 *   id = "codepen",
 *   label = @Translation("Codepen embed widget"),
 *   field_types = {
 *     "codepen"
 *   },
 * )
 */
class CodepenDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder_url' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['placeholder_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for URL'),
      '#default_value' => $this->getSetting('placeholder_url'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholder_url = $this->getSetting('placeholder_url');
    if (empty($placeholder_url)) {
      $summary[] = $this->t('No placeholders');
    }
    else {
      if (!empty($placeholder_url)) {
        $summary[] = $this->t('URL placeholder: @placeholder_url', ['@placeholder_url' => $placeholder_url]);
      }
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['input'] = $element + [
      '#type' => 'textfield',
      '#placeholder' => $this->getSetting('placeholder_url'),
      '#default_value' => isset($items[$delta]->input) ? $items[$delta]->input : NULL,
      '#maxlength' => 255,
      '#element_validate' => [[$this, 'validateInput']],
    ];

    if ($element['input']['#description'] == '') {
      $element['input']['#description'] = $this->t('Enter the Codepen URL. Valid URL format: http://www.codepen.com/user/pen/hash');
    }

    $element['codepen_html'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable HTML tab'),
      '#default_value' => isset($items[$delta]->codepen_html) ? $items[$delta]->codepen_html : FALSE,
    ];

    $element['codepen_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CSS tab'),
      '#default_value' => isset($items[$delta]->codepen_css) ? $items[$delta]->codepen_css : FALSE,
    ];

    $element['codepen_js'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable JS tab'),
      '#default_value' => isset($items[$delta]->codepen_js) ? $items[$delta]->codepen_js : FALSE,
    ];

    $element['codepen_result'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable result tab'),
      '#default_value' => isset($items[$delta]->codepen_result) ? $items[$delta]->codepen_result : FALSE,
    ];

    if (isset($items->get($delta)->codepen_id)) {
      $element['codepen_id'] = [
        '#prefix' => '<div class="codepen-codepen-id">',
        '#markup' => $this->t('Codepen embed ID: @codepen_id', ['@codepen_id' => $items->get($delta)->codepen_id]),
        '#suffix' => '</div>',
        '#weight' => 1,
      ];
    }

    if (isset($items->get($delta)->user_id)) {
      $element['user_id'] = [
        '#prefix' => '<div class="codepen-user-id">',
        '#markup' => $this->t('Codepen user ID: @user_id', ['@user_id' => $items->get($delta)->user_id]),
        '#suffix' => '</div>',
        '#weight' => 1,
      ];
    }
    return $element;
  }

  /**
   * Validate Codepen URL.
   */
  public function validateInput(&$element, FormStateInterface &$form_state, $form) {
    $input = $element['#value'];
    $codepen_id = codepen_get_codepen_id($input);
    $user_id = codepen_get_user_id($input);

    if ($codepen_id && $user_id && strlen($codepen_id) <= 20) {
      $codepen_id_element = [
        '#parents' => $element['#parents'],
      ];
      $user_id_element = [
        '#parents' => $element['#parents'],
      ];
      array_pop($codepen_id_element['#parents']);
      array_pop($user_id_element['#parents']);
      $codepen_id_element['#parents'][] = 'codepen_id';
      $user_id_element['#parents'][] = 'user_id';
      $form_state->setValueForElement($codepen_id_element, $codepen_id);
      $form_state->setValueForElement($user_id_element, $user_id);
    }
    elseif (!empty($input)) {
      $form_state->setError($element, $this->t('Please provide a valid Codepen URL.'));
    }
  }

}
