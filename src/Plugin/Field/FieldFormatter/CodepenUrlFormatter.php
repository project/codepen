<?php

namespace Drupal\codepen\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'codepen_url' formatter.
 *
 * @FieldFormatter(
 *   id = "codepen_url",
 *   label = @Translation("Codepen URL"),
 *   field_types = {
 *     "codepen"
 *   }
 * )
 */
class CodepenUrlFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Output this field as a link'),
      '#default_value' => $this->getSetting('link'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $link = $this->getSetting('link');

    if ($link) {
      $summary[] = $this->t('Codepen URL as a link.');
    }
    else {
      $summary[] = $this->t('Codepen URL as plain text.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {}

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $link = $this->getSetting('link');

    foreach ($items as $delta => $item) {
      if ($link) {
        $element[$delta] = [
          '#type' => 'link',
          '#title' => $item->input,
          '#url' => Url::fromUri($item->input),
          '#options' => [
            'attributes' => [
              'class' => [
                'codepen-url',
                'codepen-url--' . Html::getClass($item->codepen_id) . '-' . Html::getClass($item->user_id),
              ],
            ],
            'html' => TRUE,
          ],
        ];
      }
      else {
        $element[$delta] = [
          '#markup' => Html::escape($item->input),
        ];
      }
    }

    return $element;
  }

}
