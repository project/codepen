<?php

namespace Drupal\codepen\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'codepen_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "codepen_embed",
 *   label = @Translation("Codepen embed"),
 *   field_types = {
 *     "codepen"
 *   }
 * )
 */
class CodepenFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'codepen_size' => '400',
      'codepen_height' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['codepen_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Codepen embed size'),
      '#options' => codepen_size_options(),
      '#default_value' => $this->getSetting('codepen_size'),
    ];
    $elements['codepen_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#size' => 10,
      '#default_value' => $this->getSetting('codepen_height'),
      '#states' => [
        'visible' => [
          ':input[name*="codepen_size"]' => ['value' => 'custom'],
        ],
      ],
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $cp = '';
    $codepen_size = $this->getSetting('codepen_size');

    $summary[] = $this->t('Codepen embed: @codepen_size@cp', ['@codepen_size' => $codepen_size, '@cp' => $cp]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {}

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'codepen_embed',
        '#input' => $item->input,
        '#codepen_default_tabs' => [
          'html' => $item->codepen_html,
          'css' => $item->codepen_css,
          'js' => $item->codepen_js,
          'result' => $item->codepen_result,
        ],
        '#codepen_id' => $item->codepen_id,
        '#user_id' => $item->user_id,
        '#entity_title' => $items->getEntity()->label(),
        '#settings' => $settings,
      ];

      $element[$delta]['#attached']['library'][] = 'codepen/drupal.codepen.css';

      if ($settings['codepen_size'] == 'responsive') {
        $element[$delta]['#attached']['library'][] = 'codepen/drupal.codepen.responsive';
      }
    }
    return $element;
  }

}
