<?php

namespace Drupal\codepen\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'codepen' field type.
 *
 * @FieldType(
 *   id = "codepen",
 *   label = @Translation("Codepen embed"),
 *   description = @Translation("This field stores a Codepen embed in the database."),
 *   default_widget = "codepen",
 *   default_formatter = "codepen_embed"
 * )
 */
class CodepenItem extends FieldItemBase {

  /**
   * Definitions of the contained properties.
   *
   * @var array
   */
  public static $propertyDefinitions;

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'input' => [
          'description' => 'Codepen URL.',
          'type' => 'varchar',
          'length' => 1024,
          'not null' => FALSE,
        ],
        'codepen_id' => [
          'description' => 'Codepen ID.',
          'type' => 'varchar',
          'length' => 20,
          'not null' => FALSE,
        ],
        'user_id' => [
          'description' => 'User ID.',
          'type' => 'varchar',
          'length' => 40,
          'not null' => FALSE,
        ],
        'codepen_html' => [
          'description' => 'HTML.',
          'type' => 'int',
          'not null' => FALSE,
        ],
        'codepen_css' => [
          'description' => 'CSS.',
          'type' => 'int',
          'not null' => FALSE,
        ],
        'codepen_js' => [
          'description' => 'JS.',
          'type' => 'int',
          'not null' => FALSE,
        ],
        'codepen_result' => [
          'description' => 'Result.',
          'type' => 'int',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['input'] = DataDefinition::create('string')
      ->setLabel(t('Codepen URL'));

    $properties['codepen_id'] = DataDefinition::create('string')
      ->setLabel(t('Codepen ID'));

    $properties['user_id'] = DataDefinition::create('string')
      ->setLabel(t('User ID'));

    $properties['codepen_html'] = DataDefinition::create('boolean')
      ->setLabel(t('HTML'));

    $properties['codepen_css'] = DataDefinition::create('boolean')
      ->setLabel(t('CSS'));

    $properties['codepen_js'] = DataDefinition::create('boolean')
      ->setLabel(t('JS'));

    $properties['codepen_result'] = DataDefinition::create('boolean')
      ->setLabel(t('Result'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('input')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'input';
  }

}
