<?php

namespace Drupal\codepen\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Codepen settings for this site.
 */
class CodepenSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'codepen_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['codepen.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('codepen.settings');
    $form['text'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('The following settings will be used as default values on all Codepen embed fields.') . '</p>',
    ];
    $form['codepen_global'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Codepen parameters'),
    ];

    $form['codepen_global']['codepen_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable light theme'),
      '#default_value' => $config->get('codepen_theme'),
      '#description' => $this->t('Themes allow you to style Embedded Pens. Need to create or edit your themes? Go to the <a href="https://codepen.io/embed-theme-builder">Embed Theme Builder</a>.'),
    ];

    $form['codepen_global']['codepen_preview'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use click-to-load.'),
      '#default_value' => $config->get('codepen_preview'),
      '#description' => $this->t('The result in Embedded Pens can either load right away, or be in a preview state where they need to be clicked to load, which can be good for performance.'),
    ];

    $form['codepen_global']['codepen_editable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Make Code Editable.'),
      '#default_value' => $config->get('codepen_editable'),
      '#description' => $this->t('You can make the code in this Embedded Pen editable, meaning you can change code and see the results, just like in the Pen Editor here. Editable Embeds require more resources than non-editable Embeds.'),
    ];

    $form['codepen_container_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Codepen container class'),
      '#default_value' => $config->get('codepen_container_class'),
      '#description' => $this->t('Set a custom class on the Codepen embed container.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory->getEditable('codepen.settings')
      ->set('codepen_theme', $values['codepen_theme'])
      ->set('codepen_preview', $values['codepen_preview'])
      ->set('codepen_editable', $values['codepen_editable'])
      ->set('codepen_container_class', $values['codepen_container_class'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
