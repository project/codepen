SUMMARY - Codepen Field
========================
The Codepen field module provides a simple field that allows you to add a
Codepen embed to a content type, user, or any entity.

This module is a stripped and altered version of the YouTube module. See
https://www.drupal.org/project/youtube. A big thank you to them for their work.

Display types include:

 * Codepen embeds of with options.


REQUIREMENTS
-------------
All dependencies of this module are enabled by default in Drupal 8.x.


INSTALLATION
-------------
Install this module as usual. Please see
http://drupal.org/documentation/install/modules-themes/modules-8

Some pointers:
Use composer or download module manually: composer require drupal/codepen
Enable module in the Drupal interface or: drush en codepen
Go to /admin/structure/types > Manage fields > Add field > Codepen Embed

Config can be found here /admin/config/media/codepen

USAGE
-------
To use this module create a new field of type 'Codepen embed'. This field will
accept Codepen URLs of the following formats:

 * http://codepen.com/[user_id]/pen/[codepen_id]

The format listed above can also be provided without 'http://', with 'www.',
or with 'https://' rather than 'http://'. The last format can be provided with
additional parameters but are ignored.


CONFIGURATION
--------------
Global module settings can be found at admin/config/media/codepen.

The codepen output of a Codepen field can be manipulated in three ways:
 * global parameters found on the configuration page mentioned above
 * field-specific parameters found in that particular field's display settings
 * Views settings for the specific field

To configure the field settings:

 1. click 'manage display' on the listing of entity types
 2. click the configuration gear to the right of the Codepen field


SUPPORT
--------
Please use the issue queue to report bugs or request support:
http://drupal.org/project/issues/codepen
