<?php

/**
 * @file
 * Codepen field module adds a field for Codepen embeds.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function codepen_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.codepen':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Codepen module provides a field that allows you to add a Codepen.io embed to a content type, user, or any other Drupal entity.') . '</p>';
      $output .= '<p>' . t('Visit the <a href=":project_link">Project page</a> on Drupal.org for more information.', [
        ':project_link' => 'https://www.drupal.org/project/codepen',
      ]);

      return $output;
  }
}

/**
 * Implements hook_menu_link_defaults().
 */
function codepen_menu_link_defaults() {
  $links = [];
  $links['codepen.settings'] = [
    'link_title' => 'Codepen Field settings',
    'description' => 'Configure global settings for Codepen fields.',
    'route_name' => 'codepen.settings',
    'parent' => 'system.admin.config.media',
  ];

  return $links;
}

/**
 * Extracts the $user_id from the submitted field value.
 *
 * @param string $input
 *   The input submitted to the field.
 *
 * @return string|bool
 *   Returns the $user_id if available, or FALSE if not.
 */
function codepen_get_user_id($input) {
  // See README.txt for accepted URL formats.
  preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:codepen\.io\/([^&]+)\/pen\/([^]+))(:?([^&\"'<> ]+))/", $input, $matches);

  if (!empty($matches[1])) {
    return $matches[1];
  }

  return FALSE;
}

function codepen_get_codepen_id($input) {
  // See README.txt for accepted URL formats.
  preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:codepen\.io\/([^&]+)\/pen\/([^]+))(:?([^&\"'<> ]+))/", $input, $matches);

  if (!empty($matches[2])) {
    return $matches[2];
  }

  return FALSE;
}

/**
 * Returns a list of standard Codepen embed sizes.
 */
function codepen_size_options() {
  return [
    '300' => '300px',
    '400' => '400px',
    'responsive' => 'responsive (full-width of container)',
    'custom' => 'custom',
  ];
}

/**
 * Implements hook_theme().
 */
function codepen_theme($existing, $type, $theme, $path) {
  return [
    'codepen_embed' => [
      'variables' => [
        'input' => NULL,
        'user_id' => NULL,
        'codepen_id' => NULL,
        'codepen_default_tabs' => [
          'html' => NULL,
          'css' => NULL,
          'js' => NULL,
          'result' => NULL,
        ],
        'entity_title' => NULL,
        'settings' => [],
        'alternative_content' => NULL,
      ],
    ],
  ];
}

/**
 * Prepares variables for the Codepen embed template.
 *
 * Default template: codepen-embed.html.twig.
 */
function template_preprocess_codepen_embed(&$variables) {
  $config = \Drupal::config('codepen.settings');

  // Add global Codepen module configuration to the settings array.
  $variables['settings'] += [
    'codepen_theme' => $config->get('codepen_theme'),
    'codepen_preview' => $config->get('codepen_preview'),
    'codepen_editable' => $config->get('codepen_editable'),
    'codepen_container_class' => $config->get('codepen_container_class'),
  ];

  $default_tabs = [];
  foreach ($variables['codepen_default_tabs'] as $key => $tab_setting) {
    if ($tab_setting) {
      $default_tabs[] = $key;
    }
  }

  if ($variables['settings']['codepen_theme']) {
    $variables['content_attributes']['data-theme-id'] = 'light';
  }

  if ($variables['settings']['codepen_preview']) {
    $variables['content_attributes']['data-preview'] = 'true';
  }

  if ($variables['settings']['codepen_editable']) {
    $variables['content_attributes']['data-editable'] = 'true';
  }

  // Use the module's privacy configuration to determine the domain.
  $variables['url'] = 'https://www.codepen.io/' . $variables['user_id'] . '/pen/' . $variables['codepen_id'];

  $variables['content_attributes']['data-slug-hash'] = $variables['codepen_id'];
  $variables['content_attributes']['data-user'] = $variables['user_id'];
  $variables['content_attributes']['data-default-tab'] = join(',', $default_tabs);

  $size = $variables['settings']['codepen_size'];
  if ($size == 'custom') {
    $height = $variables['settings']['codepen_height'];
    $variables['height'] = $height;
    $variables['content_attributes']['data-height'] = $height;
  }
  else {
    if ($size != 'responsive') {
      $variables['height'] = $size;
      $variables['content_attributes']['data-height'] = $size;
    }
  }

  $variables['content_attributes']['class'][] = 'codepen';
  // Add classes to the wrapping element.
  $variables['attributes']['class'][] = 'codepen-container';
  $variables['attributes']['class'][] = $variables['settings']['codepen_container_class'];
  if ($size == 'responsive') {
    $variables['attributes']['class'][] = 'codepen-container--responsive';
  }
}

